<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function Register(){
        return view('form');
    }

    public function Kirim(Request $request){
        // dd($request->all());
        $firstName = $request['first_name'];
        $lastName = $request['last_name'];
        return view('welcome',compact('firstName', 'lastName'));

    }

    public function Welcome(){
        return view('welcome');
    }
}
